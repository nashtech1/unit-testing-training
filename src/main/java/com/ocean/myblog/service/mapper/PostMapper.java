package com.ocean.myblog.service.mapper;


import com.ocean.myblog.domain.*;
import com.ocean.myblog.service.dto.PostDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Post} and its DTO {@link PostDTO}.
 */
@Mapper(componentModel = "spring", uses = {AuthorMapper.class})
public interface PostMapper extends EntityMapper<PostDTO, Post> {

    @Mapping(source = "author.id", target = "authorId")
    PostDTO toDto(Post post);

    @Mapping(source = "authorId", target = "author")
    Post toEntity(PostDTO postDTO);

    default Post fromId(Long id) {
        if (id == null) {
            return null;
        }
        Post post = new Post();
        post.setId(id);
        return post;
    }
}
