/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ocean.myblog.web.rest.vm;
